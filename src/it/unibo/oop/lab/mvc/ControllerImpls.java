package it.unibo.oop.lab.mvc;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Implementation of Controller.
 *
 *
 */
public final class ControllerImpls implements Controller {

    private final List<String> strings = new LinkedList<>();
    private String nextStringToPrint = new String();

    @Override
    public void setNextString(final String nextOne) {
        nextStringToPrint = Objects.requireNonNull(nextOne, "This method does not accept null values");

    }

    @Override
    public String getNextString() {
        return nextStringToPrint;
    }

    @Override
    public List<String> pastStrings() {
        return new LinkedList<>(strings);
    }

    @Override
    public void printString() {
        if (nextStringToPrint.isEmpty()) {
            throw new IllegalStateException();
        } else {
            System.out.println(nextStringToPrint);
            strings.add(nextStringToPrint);
            nextStringToPrint = new String();
        }
    }

}
