package it.unibo.oop.lab.mvcio;

import java.io.File;
import java.io.IOException;

/**
 *
 */
public class Controller {

    /*
     * This class must implement a simple controller responsible of I/O access. It
     * considers a single file at a time, and it is able to serialize objects in it.
     *
     * Implement this class with:
     *
     * 1) A method for setting a File as current file
     *
     * 2) A method for getting the current File
     *
     * 3) A method for getting the path (in form of String) of the current File
     *
     * 4) A method that gets a String as input and saves its content on the current
     * file. This method may throw an IOException.
     *
     * 5) By default, the current file is "output.txt" inside the user home folder.
     * A String representing the local user home folder can be accessed using
     * System.getProperty("user.home"). The separator symbol (/ on *nix, \ on
     * Windows) can be obtained as String through the method
     * System.getProperty("file.separator"). The combined use of those methods leads
     * to a software that runs correctly on every platform.
     */
    private File target;
    private static final String SEPARATOR = System.getProperty("file.separator");

    /**
     * Default value for target is home/output.txt .
     *
     */
    public Controller() {
        target = new File(System.getProperty("user.home") + SEPARATOR + "output.txt");
    }

    /**
     * Set the current file as the new one. Note that a defensive copy is created.
     * @param newFile the target which is going to be the new one
     */
    public void setFile(final File newFile) {
        target = new File(newFile.getAbsolutePath());
    }

    /**
     * Set the file target with the new one.
     * @param path per.
     * @throws IOException se non c'è il file.
     */
    public void save(final String path) throws IOException {
        target = new File(path);
    }
    /**
     * Defensive copy is created.
     * @return the current saved file.
     */
    public File getFile() {
        return new File(target.getAbsolutePath());
    }

    /**
     *
     * @return the path of string
     */
    public String getPath() {
        return target.getAbsolutePath();
    }
}
