package it.unibo.oop.lab.mvcio;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.PrintStream;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * A very simple program using a graphical interface.
 *
 */
public final class SimpleGUI {

    private final JFrame frame = new JFrame();
    private final Controller saver = new Controller();

    /*
     * Once the Controller is done, implement this class in such a way that:
     *
     * 1) I has a main method that starts the graphical application
     *
     * 2) In its constructor, sets up the whole view
     *
     * 3) The graphical interface consists of a JTextArea with a button "Save" right
     * below (see "ex02.png" for the expected result). SUGGESTION: Use a JPanel with
     * BorderLayout
     *
     * 4) By default, if the graphical interface is closed the program must exit
     * (call setDefaultCloseOperation)
     *
     * 5) The program asks the controller to save the file if the button "Save" gets
     * pressed.
     *
     * Use "ex02.png" (in the res directory) to verify the expected aspect.
     */

    /**
     * builds a new {@link SimpleGUI}.
     */
    public SimpleGUI() {
        /*
         * Make the frame half the resolution of the screen. This very method is enough
         * for a single screen setup. In case of multiple monitors, the primary is
         * selected.
         *
         * In order to deal coherently with multimonitor setups, other facilities exist
         * (see the Java documentation about this issue). It is MUCH better than
         * manually specify the size of a window in pixel: it takes into account the
         * current resolution.
         */
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        /*
         * Instead of appearing at (0,0), upper left corner of the screen, this flag
         * makes the OS window manager take care of the default positioning on screen.
         * Results may vary, but it is generally the best choice.
         */
        frame.setLocationByPlatform(true);

        final JPanel canvas = new JPanel(new BorderLayout());
        final JTextArea text = new JTextArea();
        final JButton save = new JButton("Save!");
        save.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                final int n = JOptionPane.showConfirmDialog(frame, "Are you sure you want to save the file ?", "Saving",
                        JOptionPane.YES_NO_OPTION);
                if (n == JOptionPane.YES_OPTION) {
                    try (PrintStream ps = new PrintStream(saver.getPath())) {
                        ps.print(text.getText());
                    } catch (final IOException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }
                }
            }
        });
        canvas.add(save, BorderLayout.SOUTH);
        canvas.add(text);

        final JPanel northPart = new JPanel(new BorderLayout());
        canvas.add(northPart, BorderLayout.NORTH);

        final JButton browse = new JButton("Browse");
        final TextField path = new TextField(saver.getPath());
        browse.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                final JFileChooser newFileChoser = new JFileChooser();
                try {
                    if (newFileChoser.showSaveDialog(browse) == JFileChooser.APPROVE_OPTION) {
                        saver.setFile(newFileChoser.getSelectedFile());
                        path.setText(saver.getPath());

                    }
                } catch (final Exception erorr) { // mettere dialog JOptionPane.show....
                    System.out.println("An error has occured");
                }
            }
        });

        northPart.add(browse, BorderLayout.LINE_END);
        northPart.add(path);
        frame.add(canvas);
    }

    /**
     * Display the frame.
     */
    public void display() {
        frame.setVisible(true);
    }

    /**
     * main.
     *
     * @param args
     *            do nothing.
     */
    public static void main(final String[] args) {
        final SimpleGUI temp = new SimpleGUI();
        temp.display();
    }

}
