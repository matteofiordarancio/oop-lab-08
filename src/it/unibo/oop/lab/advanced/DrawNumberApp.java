package it.unibo.oop.lab.advanced;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 */
public final class DrawNumberApp implements DrawNumberViewObserver {
    private final DrawNumber model;
    private final DrawNumberView[] views = new DrawNumberView[3];

    /**
     *
     */
    public DrawNumberApp() {
        String line;
        int min = 0;
        int max = 0;
        int attempts = 0;
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(ClassLoader.getSystemResourceAsStream("config.yml")))) {
            line = br.readLine();
            if (line != null) {
                min = Integer.parseInt(line.split(": ")[1]);
            }
            line = br.readLine();
            if (line != null) {
                max = Integer.parseInt(line.split(": ")[1]);
            }
            line = br.readLine();
            if (line != null) {
                attempts = Integer.parseInt(line.split(": ")[1]);
            }
        } catch (final IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        model = new DrawNumberImpl(min, max, attempts);
        views[0] = new DrawNumberViewImpl();
        views[1] = new DrawNumberViewImplFile();
        views[2] = new DrawNumberViewOnStdout();
        for (final DrawNumberView drawNumberView : views) {
            drawNumberView.setObserver(this);
            drawNumberView.start();
        }
    }

    @Override
    public void newAttempt(final int n) {
        try {
            final DrawResult result = model.attempt(n);
            for (final DrawNumberView drawNumberView : views) {
                drawNumberView.result(result);
            }
        } catch (final IllegalArgumentException e) {
            for (final DrawNumberView drawNumberView : views) {
                drawNumberView.numberIncorrect();
            }
        } catch (final AttemptsLimitReachedException e) {
            for (final DrawNumberView drawNumberView : views) {
                drawNumberView.limitsReached();
            }
        }
    }

    @Override
    public void resetGame() {
        model.reset();
    }

    @Override
    public void quit() {
        System.exit(0);
    }

    /**
     * @param args
     *            ignored
     */
    public static void main(final String... args) {
        new DrawNumberApp();
    }

}
