package it.unibo.oop.lab.advanced;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * On file.
 *
 */
public final class DrawNumberViewImplFile extends DrawNumberViewImpl {

    private static final String NEW_GAME = ": a new game starts!";

    @Override
    public void result(final DrawResult res) {
        try (BufferedWriter br = new BufferedWriter(new FileWriter(new File("output.txt")))) {
            switch (res) {
            case YOURS_HIGH:
            case YOURS_LOW:
                br.append(res.getDescription());
                return;
            case YOU_WON:
                br.append(res.getDescription() + NEW_GAME);
                break;
            default:
                throw new IllegalStateException("Unexpected result: " + res);
            }
        } catch (final IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void numberIncorrect() {
        try (BufferedWriter br = new BufferedWriter(new FileWriter(new File("output.txt")))) {
            br.append("Incorrect Number.. try again");
        } catch (final IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void limitsReached() {
        try (BufferedWriter br = new BufferedWriter(new FileWriter(new File("output.txt")))) {
            br.append("You lost" + NEW_GAME);
        } catch (final IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
